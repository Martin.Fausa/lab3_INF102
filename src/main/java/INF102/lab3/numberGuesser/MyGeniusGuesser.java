package INF102.lab3.numberGuesser;

public class MyGeniusGuesser implements IGuesser {

    private int lowerbound;
    private int upperbound;

    @Override
    public int findNumber(RandomNumber number) {
        lowerbound = number.getLowerbound();
        upperbound = number.getUpperbound();

        int numberGuess;
        do {
            numberGuess = makeGuess();
            ///////////////////
            // Ved å bruke koden som allerede er skrevet i RandomGuesser.java
            // kan vi modifisere den slik at den ikke tipper tilfeldig, men fortsetter å dele lista utifra
            // om gjettet er mindre eller større enn fasitsvaret. Dermed vil den halvere antall mulige tall 
            // å velge mellom i den retningen som det riktige svaret befinner seg i. Dette er altså et binærsøk.
            // 1 = for høyt : dermed er upperbound et tall under det vi gjettet med.
            // -1 = for lavt: dermed er lowerbound et tall over det vi gjettet med
            int guessOutcome = number.guess(numberGuess);
            if (guessOutcome == 1) {
                upperbound = numberGuess - 1;
            } else if (guessOutcome == -1) {
                lowerbound = numberGuess + 1;
            }
            // while-loopen fortsetter å utføre makeGuess()-metoden som halverer summen av lowerbound og upperbound.
            // Når vi får 0 returnert avsluttes loopen og vi har funnet fasit.
            ///////////////////
            
        } while (number.guess(numberGuess) != 0);

        return numberGuess;
    }

    private int makeGuess() {
        return (upperbound+lowerbound) / 2;
    }
}
