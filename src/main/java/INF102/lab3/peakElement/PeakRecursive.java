package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if (numbers.isEmpty()) {
            throw new IllegalArgumentException("Empty list");
        }
        int firstIndex = 0;
        int lastIndex = (numbers.size() - 1);
        
        return findPeak(numbers, firstIndex, lastIndex);
    }

    private int findPeak(List<Integer> numbers, int left, int right) {
        if (left == right) {
            // Returnerer peak.
            return numbers.get(left);
        }
        // Vi kan gjøre et binærsøk og starte fra midten og dividere indeks-tallet vi har hver gang i den retning hvor
        // tallet er mindre enn de ved siden av seg på listen.
        int mid = (right + left) / 2;
        if (numbers.get(mid) > numbers.get(mid + 1)) {
            return findPeak(numbers, left, mid);
        } else {
            return findPeak(numbers, mid + 1, right);
        }
    } 
}