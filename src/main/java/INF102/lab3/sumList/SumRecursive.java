package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        return sumList(list, 0);
    }
    // Vi lager en hjelpemetode som rekursivt går gjennom elementene i listen ved å øke indeks hver gang,
    // og returnerer summen av disse.
    // Ved å ikke lage en kopi av listen og jobbe utifra den, vil den konsekvent passere testene om at 
    // den er raskere enn sumIterative.  
    private long sumList(List<Long> list, int index) {
        if (list.isEmpty()) {                                     
            return 0;
        }
        if (index >= list.size()) {
            return 0;
        } else {
            long currentElement = list.get(index);
            return currentElement + sumList(list, index + 1);
        }
    }

}
